type Gender = 'male' | 'female' | 'others'
type Roles = 'admin' | 'user'
type User = {
    id: number
    email: string
    password: string
    fullName: string
    gender: Gender // Male, Female, Others
    roles: string[] // admin, user
}

export type { Gender, Roles, User }