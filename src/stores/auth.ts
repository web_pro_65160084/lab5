import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
    const currentUser = ref<User>({
    id: 1,
    email: 'Mana@gmail.com',
    password: 'Pass@123',
    fullName: 'มานะ งานดี',
    gender: 'male',
    roles: ['user']
})

  return { currentUser }
})
